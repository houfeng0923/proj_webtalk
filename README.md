webtalk demo
### how to demo
**prerequisite** nodejs

>npm install

>npm start

visit http://localhost:3000
then input a number*(from 1 to 4)*  as userid which is provided by backend.
and then login
**or**
directly visit http://localhost:3000/{{userid}}

open multi windows to talkto eachother

### tips
script is written in [angularjs framework](https://angularjs.org/)

module system is [AMD](https://github.com/amdjs/amdjs-api/wiki/AMD), and module loader is [requirejs](http://www.requirejs.org/)

style is base on [bootstrap](http://getbootstrap.com/)

front end template is [jade](http://jade-lang.com/)

if u don't like jade, here a grunt script is provided to compile it to html.

just run
>grunt

then look the `dest/` folder

### usage
**if u plant webtalk page into ur app ** 

a variable `{userid}` is required to render the page, which need it for running the scripts correctly. And it's type should be int.

all interactions with the backend is done by `userService` and `msgService`, they are in the `public/javascripts/services/` folder. just seek and modify the url there.

module config file is `public/javascripts/main.js`, webtalk page's entry is `public/javascripts/bootstrap_webtalk.js`

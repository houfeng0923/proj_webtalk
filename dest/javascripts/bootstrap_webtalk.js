define(['require','angular','app/webtalk'],
    function(require, ng) {
  'user strict';
  
  require(['domReady!'], function(document) {
    ng.bootstrap(document,['webtalkApp']);
  });
});
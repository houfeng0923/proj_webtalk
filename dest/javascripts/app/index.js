define(['angular','controllers/index'],
    function(ng) {
  'use strict';

  return ng.module('webtalkApp',['app.controllers']);
})
define(['./module'], function(services) {
  'use strict';

  services.factory('userService', [
      '$http','$rootScope','$window',
      function($http,$rootScope,$window) {
    var service = {};
    var currentUser;
    var facing;
    var userlist;

    service.getCurrentUser = function() {
      return currentUser;
    }

    service.talkTo = function(userid) {
      facing = userid;
      service.broadcastItem();
    };

    service.getFacingUser = function() {
      return facing;
    }

    service.getUser = function(userid) {
      if (!userid)
        throw new Error('userid must be provided.');
      if (typeof userid != 'number')
        throw new Error('type error: user id must be a number');
      if (!userlist)
        throw new Error('user list is empty.');
      var userObj = {};
      userlist.some(function(user) {
        if ((user.id)!=(userid))
          return false;
        angular.copy(user, userObj);
        userObj.id = userObj.id;
        return true;
      })
      return userObj;
    }

    service.broadcastItem = function() {
      $rootScope.$broadcast('onTalkTo');
    };

    service.startup = function() {
      // {
      // "id":""
      // "username": "admin", 
      // "status": 1, //0:离线，1:在线
      // "ﬁrstname": null,
      // "lasttime": 1414019136, 
      // "avatar": null, 
      // "lastname": null, 
      // }
      currentUser = $window.currentUser;
      $http.get('/api/users?userid='+currentUser)
          .success(function(data,status,headers,config) {
            console.log(data);
            data.forEach(function(user) {
              if (user.lasttime != null)
                user.lasttime = new Date(user.lasttime);
            });
            $rootScope.$broadcast('users.refresh',data);
            userlist = data;
          })
          .error(function(data,status,headers,config) {
            console.log(data);
          });
      setInterval(function(){
        $http.get('/api/users?userid='+currentUser)
          .success(function(data,status,headers,config) {
            console.log(data);
            $rootScope.$broadcast('users.refresh',data);
          })
          .error(function(data,status,headers,config) {
            console.log(data);
          });
      },10*1000);
    };

    return service;
  }]);
});
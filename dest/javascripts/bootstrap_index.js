define(['require','angular','app/index'],
    function(require, ng) {
  'user strict';
  
  require(['domReady!'], function(document) {
    ng.bootstrap(document,['webtalkApp']);
  });
});
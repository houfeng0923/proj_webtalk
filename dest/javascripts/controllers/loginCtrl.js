define(['./module'], function(controllers) {
  'use strict';

  controllers.controller('loginCtrl',
      ['$scope','$location',function($scope,$location) {
    $scope.placeholder = 'input your name here';
    $scope.userid = '';
    $scope.action = '/webtalk/guest';
  }]);
});
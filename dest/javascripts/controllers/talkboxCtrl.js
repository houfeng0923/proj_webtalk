define(['./module'], function(controllers) {
  'use strict';

  controllers.controller('talkboxCtrl', [
      '$scope','$http','$sce','msgService','userService',
      function($scope,$http,$sce,msgService,userService) {
    $scope.msgToPost = '';
    $scope.talkTo = null;
    $scope.chats = [];
    $scope.show = false;

    $scope.$on('onTalkTo', function() {
      $scope.talkTo = userService.getFacingUser();
      $scope.chats.splice(0, $scope.chats.length);
      $scope.show = true;
      var msgObj;
      while(msgObj=msgService.readMsg($scope.talkTo)) {
        msgObj._class = 'other';
        msgObj.user = userService.getUser(msgObj.send_user_id);
        $scope.chats.push(msgObj);
      }
    });

    $scope.$on('message', function(e, msgObj) {
      if ($scope.talkTo) {
        var msg = msgService.readMsg($scope.talkTo);
        if (msg == undefined) {
          console.warn('message service may failed on delivering!');
        } else {
          msg.user = userService.getUser(msg.send_user_id);
          $scope.chats.push(msg);
        }
      }
    });

    $scope.stopTalking = function() {
      $scope.talkTo = null;
      $scope.show = false;
    };

    $scope.getFacingUserName = function() {
      if ($scope.talkTo == undefined)
        return '';
      var user = userService.getUser($scope.talkTo);
      return user.firstname+user.lastname;
    }

    $scope.send = function() {
      if (!$scope.msgToPost || !$scope.talkTo) {
        return;
      }
      var currentUser = userService.getCurrentUser();
      msgService.sendMsg({
        recv_user_id:$scope.talkTo,
        send_user_id:currentUser,
        content:$scope.msgToPost
      },function(data,status,headers,config) {
        $scope.chats.push({
          recv_user_id:$scope.talkTo,
          send_user_id:currentUser,
          user:userService.getUser(currentUser),
          content:$scope.msgToPost,
          _class:'me'
        });
        $scope.msgToPost = '';
      },function(data, status, headers, config) {
      });
      console.log('post: '+$scope.msgToPost);
    };

  }]);
});
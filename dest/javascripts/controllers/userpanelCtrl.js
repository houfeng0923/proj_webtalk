define(['angular','./module'], function(angular,controllers) {
  'use strict';
  
  controllers.controller('userpanelCtrl', [
      '$scope','$timeout','userService',
      function($scope,$timeout,userService) {
    $scope.offlineUsers = [];
    $scope.onlineUsers = [];
    $scope.select = function(userid) {
      userService.talkTo(userid);
    };
    $scope.$on('message', function(e, msgObj) {
      
    });
    $scope.$on('users.refresh',function(e,users) {
      var currentUser = userService.getCurrentUser();
      var list = users.filter(function(user) {
        user.id = user.id;
        return !(user.id==currentUser);
      });
      var online = list.filter(function(user) {
        return user.status;
      });
      var offline = list.filter(function(user) {
        return !user.status;
      });
      if (!angular.equals($scope.onlineUsers, online)) {
        $scope.onlineUsers = online;
      }
      if (!angular.equals($scope.offlineUsers, offline)) {
        $scope.offlineUsers = offline;
      }
    });
  }]);
});
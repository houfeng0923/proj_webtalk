define(['angular','ngSanitize',
  'controllers/webtalk','services/webtalk'
  ],
    function(ng) {
  'use strict';

  var webtalk = ng.module('webtalkApp',['ngSanitize','app.controllers','app.services'])
    .config(function() {
    })
    .run(['msgService','userService',function(msgService,userService) {
      msgService.startup();
      userService.startup();
    }]);

  return webtalk;
})
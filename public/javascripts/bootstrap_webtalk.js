define(['angular','app/webtalk'],
    function(ng) {
  'user strict';

  require(['domReady!'], function(document) {
    ng.bootstrap(document,['webtalkApp']);
  });
});
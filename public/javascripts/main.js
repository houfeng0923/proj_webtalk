var boostrap = document.getElementsByTagName('body')[0].getAttribute('data-bootstrap');
if (!boostrap)
  throw new Error('This page is missing the bootstrap script.');

require.config({
  baseUrl:'/javascripts/',
  packages:[
    'app',
    'controllers'
  ],
  // alias libraries paths
  paths:{
    'domReady':'./requirejs/domReady',
    'text':'./requirejs/text',
    'jquery':'./jquery/jquery-2.1.1',
    'angular':'./angularjs/angular',
    'ngSanitize':'./angularjs/angular-sanitize'
  },
  shim:{
    'angular':{
      exports:'angular'
    },
    'ngSanitize':{
      deps: ['angular']
    }
  },
  deps:['./'+boostrap]
});
define(['./module'], function(services) {
  'use strict';

  services.factory('userService', [
      '$http','$rootScope','$window',
      function($http,$rootScope,$window) {
    var service = {};
    var currentUserId;
    var facing;
    var userlist;

    service.getCurrentUserId = function() {
      return currentUserId;
    }

    service.talkTo = function(userid) {
      facing = userid;
      $rootScope.$broadcast('onTalkTo', facing);
    };

    service.getFacingUser = function() {
      // return facing;
      return facing ? this.getUser(facing) : null;
    }

    service.getUser = function(userid) {
      if (!userid)
        throw new Error('userid must be provided.');
      if (typeof userid != 'number')
        throw new Error('type error: user id must be a number');
      if (!userlist)
        throw new Error('user list is empty.');
      var userObj = {};
      userlist.some(function(user) {
        if ((user.id)!=(userid))
          return false;
        angular.copy(user, userObj);
        // userObj.id = userObj.id;
        return true;
      })
      return userObj;
    }

    service.broadcastItem = function() {
      $rootScope.$broadcast('onTalkTo');
    };

    service.startup = function() {
      // {
      // "id":""
      // "username": "admin",
      // "status": 1, //0:离线，1:在线
      // "ﬁrstname": null,
      // "lasttime": 1414019136,
      // "avatar": null,
      // "lastname": null,
      // }
      currentUserId = $window.currentUserId;
      this.sync(currentUserId);
      setInterval(function(){
        this.sync(currentUserId);
      }.bind(this),10*1000);
    };

    service.sync = function (currentUserId){
        $http.get('/api/users?userid='+currentUserId)
          .success(function(data,status,headers,config) {
            // console.log(data);
            $rootScope.$broadcast('users.refresh',data);
            userlist = data;
          })
          .error(function(data,status,headers,config) {
            console.log(data);
          });
    }

    return service;
  }]);
});
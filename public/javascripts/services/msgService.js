define(['./module'], function(services) {
  'use strict';

  services.factory('msgService', [
      '$http','$rootScope','$window',
      function($http,$rootScope,$window) {
    var service = {};
    var inbox = {};
    var currentUserId;

    service.startup = function() {
      currentUserId = $window.currentUserId;
      // get: /api/msgs?recv_user_id=XX
      // return:
      //  [{"msg_id":XX,
      //    "send_user_id":XX,
      //    "recv_user_id":XX,
      //    "content":XX,
      //    "timestamp":XX}]
      //
      var es = new EventSource('/api/msgs/?recv_user_id='+currentUserId);
      es.onmessage = function(msg) {
        var msgObj = JSON.parse(msg.data);
        if (msgObj.send_user_id == undefined)
          throw new Error('read message error, missing param: send_user_id');
        if (typeof msgObj.send_user_id != "number")
          throw new Error('type error: send_user_id');
        if (msgObj.recv_user_id == undefined)
          throw new Error('read message error, missing param: recv_user_id');
        if (typeof msgObj.recv_user_id != "number")
          throw new Error('type error: recv_user_id');
        if (msgObj.msg_id==undefined || (msgObj.msg_id+'').trim()=="")
          throw new Error('server message format error:missing msg_id');
        if (msgObj.timestamp==undefined || (msgObj.timestamp+'').trim()=="")
          throw new Error('server message format error:missing timestamp');

        if (!inbox[msgObj.send_user_id])
          inbox[msgObj.send_user_id] = [];
        inbox[msgObj.send_user_id].push(msgObj);
        $rootScope.$broadcast('message', msgObj);
      };
      es.onopen = function(event) {
      };
      es.onerror = function(err) {
      };
    };

    // post: /api/msgs
    //  {send_user_id:XX,recv_user_id:XX,content:"XX"}
    // return:
    //  [{"msg":"success","code":0}],
    //  [{"msg":"username not exist","code":1}],
    //  [{"msg":"other error","code":2}]
    service.sendMsg = function(msgObj,onsuccess, onerror) {
      $http.post('/api/msgs', msgObj)
        .success(onsuccess)
        .error(onerror);
    };

    // put: /api/msgs/MSG_ID
    // return:
    //  [{"msg":"success","code":0}],
    //  [{"msg":"msg not exist","code":1}],
    //  [{"msg":"other error","code":2}]
    service.readMsg = function(send_user_id) {
      if (send_user_id == undefined)
        throw new Error('read message error, missing param: send_user_id');
      if (typeof send_user_id != "number")
        throw new Error('type error: send_user_id');

      if (!inbox[send_user_id])
        return null;
      var msgObj = inbox[send_user_id].shift();
      if (msgObj == undefined || msgObj == null)
        return null;

      if (typeof msgObj != 'object')
        throw new Error('msg type error!');
      if (msgObj.msg_id==undefined || (msgObj.msg_id+'').trim()=="")
        throw new Error('msg format error: missing msg_id');
      // todo performance issue
      $http.put('/api/msgs/'+msgObj.msg_id);
      return msgObj;
    };

    // delete: /api/msgs/MSG_ID
    // return:
    //  [{"msg":"success","code":0}],
    //  [{"msg":"msg not exist","code":1}],
    //  [{"msg":"other error","code":2}]
    service.deleteMsg = function() {

    };

    // get: /api/historymsgs/:userid/:senduserid
    // return:
    // {code:0,data:[{'msg_id':XX, "send_user_id":XX, "recv_user_id":XX, "content":"XX", timestamp:XX}]}
    service.getHistoryMsg = function (send_user_id){
      if (send_user_id == undefined)
        throw new Error('getHistoryMsg error, missing param: send_user_id');

      var url = '/api/historymsgs/:userid/:senduserid'.replace(':userid',currentUserId)
                .replace(':senduserid',send_user_id);
      return $http.get(url);
    }

    return service;
  }]);
});
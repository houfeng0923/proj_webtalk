define(['angular','app/index'],
    function(ng) {
  'user strict';

  require(['domReady!'], function(document) {
    ng.bootstrap(document,['webtalkApp']);
  });
});
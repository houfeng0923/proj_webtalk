define(['./module'], function(controllers) {
  'use strict';

  controllers.controller('actionCtrl', [
      '$scope','$rootScope','$http','$sce','msgService','userService',
      function($scope,$rootScope,$http,$sce,msgService,userService) {

    var talkToId = null;

    $scope.showTalkbox = false;
    $scope.showHistory = false;
    $scope.showMenu = false;


    $scope.$on('onTalkTo', function(e,userId) {
      if(userId){
        talkToId = userId;
        $scope.showTalkbox = true;
        $scope.showHistory = false;
      }
    });


    $scope.stopTalking = function() {
      $scope.showMenu = false;
      $scope.showTalkbox = false;
      $scope.showHistory = false;
      $scope.$broadcast('offTalk',talkToId);
    };

    $scope.toggleMenu = function() {
      $scope.showMenu = !$scope.showMenu;
    };

    $scope.openHistory = function (){
      $scope.showTalkbox = false;
      $scope.showHistory = true;
      $scope.$broadcast('showHistory',talkToId);
    };


    $scope.backTalkbox = function (){
      $scope.showHistory = false;
      $scope.showTalkbox = true;

    };

  }]);
});
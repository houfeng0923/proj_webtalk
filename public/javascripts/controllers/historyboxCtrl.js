define(['angular','./module'], function(angular,controllers) {
  'use strict';

  controllers.controller('historyboxCtrl', [
      '$scope','$rootScope','$timeout','msgService','userService',
      function($scope,$rootScope,$timeout,msgService,userService) {

        $scope.$on('showHistory',function (e,talkToId){
          msgService.getHistoryMsg(talkToId)
          .success(function (resp){
            $scope.addHistoryMsg(resp.data);
          })
        });


        $scope.addHistoryMsg = function (data ){
          $scope.historyMsgs = data;
        };
  }]);
});
define(['angular','./module'], function(angular,controllers) {
  'use strict';

  controllers.controller('userpanelCtrl', [
      '$scope','$timeout','userService',
      function($scope,$timeout,userService) {

    var hasMsg = {};
    var decorate = (function() {
      var decorator = function(user) {
        if(hasMsg[user.id]) {
          user._style_msg = 'msg-tip';
        } else {
          user._style_msg = 'mock';
        }
      }
      return function() {
        $scope.onlineUsers.forEach(decorator);
        $scope.offlineUsers.forEach(decorator);
      };
    })();

    $scope.offlineUsers = [];
    $scope.onlineUsers = [];
    $scope.select = function(userid) {
      userService.talkTo(userid);
      hasMsg[userid] = false;
      decorate();
    };
    $scope.$on('message', function(e, msgObj) {
      var userid = msgObj.send_user_id;
      var facingUser = userService.getFacingUser();
      if (facingUser && facingUser.id == userid) {
        // this means talkbox is open, and 'I' am talking to the msg sender.
        return;
      }
      hasMsg[userid] = true;
      decorate();
    });
    $scope.$on('users.refresh',function(e,users) {
      var currentUserId = userService.getCurrentUserId();
      var list = users.filter(function(user) {
        user.id = user.id;
        return !(user.id==currentUserId);
      });
      var online = list.filter(function(user) {
        return user.status;
      });
      var offline = list.filter(function(user) {
        return !user.status;
      });
      var refresh = false;
      if (!angular.equals($scope.onlineUsers, online)) {
        $scope.onlineUsers = online;
        refresh = true;
      }
      if (!angular.equals($scope.offlineUsers, offline)) {
        $scope.offlineUsers = offline;
        refresh = true;
      }
      if (refresh)
        decorate();
    });
  }]);
});
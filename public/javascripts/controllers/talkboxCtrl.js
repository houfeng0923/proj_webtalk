define(['./module'], function(controllers) {
  'use strict';

  controllers.controller('talkboxCtrl', [
      '$scope','$rootScope','$http','$sce','msgService','userService',
      function($scope,$rootScope,$http,$sce,msgService,userService) {
    $scope.msgToPost = '';
    $scope.talkTo = null;
    $scope.chats = [];

    $scope.$on('onTalkTo', function() {
      var talkTo = userService.getFacingUser();
      if(!talkTo){ /*debugger*/  return;  }
      $scope.talkTo = talkTo;
      $scope.chats.length = 0;
      var msgObj;
      while(msgObj=msgService.readMsg(talkTo.id)) {
        msgObj._class = 'other';
        msgObj.user = talkTo;
        $scope.chats.push(msgObj);
      }
    });

    $scope.$on('offTalk',function (){
      $scope.talkTo = null;
    });

    $scope.$on('message', function(e, msgObj) {
      if (!$scope.talkTo) {  return;  }
      var msg = msgService.readMsg($scope.talkTo.id);
      if (!msg) {
        /* 貌似正常逻辑 */
        console.warn('message service may failed on delivering!');
        return;
      }
      msg.user = $scope.talkTo ;
      $scope.chats.push(msg);

      // ??
      setTimeout(function() {
        var list = document.getElementById('msg-list');
        list.scrollTop = list.scrollHeight+200;
      });

    });


    $scope.send = function() {
      if (!$scope.msgToPost || !$scope.talkTo) {
        return;
      }
      var currentUserId = userService.getCurrentUserId();
      msgService.sendMsg({
        recv_user_id:$scope.talkTo.id,
        send_user_id:currentUserId,
        content:$scope.msgToPost
      },function(data,status,headers,config) {
        $scope.chats.push({
          recv_user_id:$scope.talkTo.id,
          send_user_id:currentUserId,
          user:userService.getUser(currentUserId),
          content:$scope.msgToPost,
          _class:'me'
        });
        $scope.msgToPost = '';
        setTimeout(function() {
          var list = document.getElementById('msg-list');
          list.scrollTop = list.scrollHeight;
        });
      },function(data, status, headers, config) {
      });
      console.log('post: '+$scope.msgToPost);
    };


  }]);
});
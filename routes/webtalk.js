var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/:userid', function(req, res) {
  res.render('webtalk', {
    title: 'Web Talk Demo',
    bootstrap: 'bootstrap_webtalk',
    userid:req.params.userid
  });
});

module.exports = router;

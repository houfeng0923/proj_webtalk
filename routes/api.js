var express = require('express');
var router = express.Router();

var userlist = [
  {id:1,status:0,username:'yangzz',firstname:'\u6768',lastname:'\u4f5c\u4ef2',lasttime:1414207010720},
  {id:2,status:0,username:'zhaob',firstname:'赵',lastname:'斌',lasttime:1414207010720},
  {id:3,status:0,username:'chenxujie',firstname:'陈',lastname:'旭杰',lasttime:1414207010720},
  {id:4,status:0,username:'lei.zhao',firstname:'赵',lastname:'磊',lasttime:1414207010720},
  {id:5,status:0,username:'zhangwb',firstname:'张',lastname:'卫滨',lasttime:1414207010720}
];
var newuser = (function() {
  var id = 0;
  return function(username,status,username,firstname,lastname,lasttime) {
    id = id+1;
    return {
      id:id,
      status:status,
      username:username,
      firstname:firstname||username,
      lastname:lastname||'',
      lasttime:lasttime||new Date().getTime()
    }
  }
})();
var filtermyself = function(myid) {
  return userlist.filter(function(user) {
    return !(user.id==myid);
  });
}
var registerOnlineState = function(userid, status) {
  //userid:string, onlne:boolean
  userlist.forEach(function(user) {
    if (user.id==userid) {
      user.status = status;
    }
  });
}
var userExist = function(userid) {
  return userlist.filter(function(user) {
    return user.id==userid;
  }).length>0;
}

var inbox = {};
var historyInbox = {};
// send_user_id":XX, "recv_user_id":XX, "content":"XX", timestamp:XX
var sendMsg = (function() {
  var id = 0;
  return function(send_user_id,recv_user_id,content,timestamp) {
    if (!recv_user_id || !userExist(recv_user_id))
      throw {msg:'recv_user_id is not exist.',type:'user'}
    if (!inbox[recv_user_id]) inbox[recv_user_id] = [];
    inbox[recv_user_id].push({
      send_user_id:send_user_id,
      recv_user_id:recv_user_id,
      content:content,
      timestamp:timestamp,
      msg_id:id
    });
    if (!historyInbox[recv_user_id]) historyInbox[recv_user_id] = [];
    historyInbox[recv_user_id].push({
      send_user_id:send_user_id,
      recv_user_id:recv_user_id,
      content:content,
      timestamp:timestamp,
      msg_id:id
    });

    id = id+1;
  }
})();
var getMsg = function(recv_user_id) {
  if (!inbox[recv_user_id] || inbox[recv_user_id].length==0)
    return null;
  return inbox[recv_user_id].shift();
};

router.get('/msgs/', function(req, res) {
  var recv_user_id = req.query.recv_user_id;
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/event-stream');
  res.setHeader('Cache-Control', 'no-cache');
  res.setHeader('Connection', 'keep-alive');
  var interval = setInterval(function() {
    var msgObj;
    while(msgObj = getMsg(recv_user_id)) {
      res.write('data:'+JSON.stringify(msgObj)+'\n\n');
    }
  });
  req.connection.addListener('close', function() {
    clearInterval(interval);
  }, false);
  // {'msg_id':XX, "send_user_id":XX, "recv_user_id":XX, "content":"XX", timestamp:XX}
});

router.post('/msgs/', function(req, res) {
  var recv_user_id = req.param('recv_user_id');
  var send_user_id = req.param('send_user_id');
  var content = req.param('content');
  try {
    sendMsg(send_user_id,recv_user_id,content,new Date().getTime());
    res.json({'msg':'success','code':0});
  } catch (err) {
    if (err.type=='user') {
      res.json({'msg':'username not exist','code':1});
    } else {
      res.json({'msg':'other error','code':2})
    }
  } finally {
    res.end();
  }
});

router.put('/msgs/:msgid',function (req,res){
  var msgid = req.params.msgid;
  for(var uid in inbox){
    var msgs = inbox[uid]||[];
    for(var i=0;i<msgs.length;i++){
      if(msgs[i].msg_id==msgid){
        msgs.splice(i,1);break;
      }
    }
  }
  res.end();
});

router.get('/users', function(req, res) {
  registerOnlineState(req.query.userid, 1);
  // res.json(filtermyself(req.query.userid));
  res.json(userlist);
  res.end();
});

router.get('/historymsgs/:userid/:senduserid',function (req,res){
  var userid = req.params.userid;
  var senduserid = req.params.senduserid;

  var msgs = [];
  var receivedMsgs = (historyInbox[userid]||[]).filter(function (msg){
    return msg.send_user_id == senduserid;
  }).map(function (msg){
    var senduser = filtermyself(msg.send_user_id)[0];
    msg['send_user'] = senduser;
    return msg;
  })
  var sendedMsgs = (historyInbox[senduserid]||[]).filter(function (msg){
    return msg.send_user_id == userid;
  }).map(function (msg){
    var senduser = filtermyself(msg.send_user_id)[0];
    msg['send_user'] = senduser;
    return msg;
  })
  msgs = receivedMsgs.concat(sendedMsgs);
  msgs.sort(function (a,b){
    return a.timestamp - b.timestamp;
  });
  res.json({
    code:0,
    data:msgs||[]
  });

  res.end();
});

// router.get('/msgs/userlist/state', function(req, res) {
//   res.statusCode = 200;
//   res.setHeader('Content-Type', 'text/event-stream');
//   res.setHeader('Cache-Control', 'no-cache');
//   res.setHeader('Connection', 'keep-alive');
//   res.write('retry: 10000\n');
//   res.write('event:connecttime\n');
//   registerOnlineState(req.query.userid, 1);
//   res.write('data: '+JSON.stringify(filtermyself(req.query.userid))+'\n\n');
//   var interval = setInterval(function() {
//     res.write('data: '+JSON.stringify(filtermyself(req.query.userid))+'\n\n');
//   });
//   req.connection.addListener('close', function() {
//     clearInterval(interval);
//     registerOnlineState(req.query.userid, 0);
//   }, false);
// });

module.exports = router;

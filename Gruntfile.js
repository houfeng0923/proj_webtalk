module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    copy: {
      main:{
        files:[
          {
            expand:true,
            cwd:'public/',
            src:['fonts/**','images/**','javascripts/**','stylesheets/**'],
            dest:'dest/'
          }
        ]
      }
    },
    jade: {
      compile: {
        options: {
          pretty:true,
          data:{
            title: 'Web Talk Demo',
            bootstrap:'bootstrap_webtalk',
            userid:'{userid}'
          }
        },
        files: {
          'dest/webtalk.html':'views/webtalk.jade'
        }
      }
    },
    less: {
      dev: {
        options: {
          yuicompress: false
        },
        files: {
          "public/stylesheets/app.css": "public/stylesheets/app/app.less"
        }
      }
    },
    watch: {
      less:{
        files:['public/stylesheets/**/*.*'],
        tasks:['less'],
        options:{
          interrupt:true
          // ,debounceDelay:100
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-contrib-copy');

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');


  grunt.registerTask('default', ['jade:compile','copy:main']);
};